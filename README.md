# Nunchaku

<p>
    <img src="https://img.shields.io/badge/status-pre--alpha-red.svg?style=flat-square" alt="development-status" />
    <img src="https://img.shields.io/badge/nimble-0.1.0-yellow.svg?style=flat-square" />
</p>

The nunchaku (Japanese: ヌンチャク Hepburn: nunchaku, often "nunchuks", "chainsticks", "chuka sticks" or "karate sticks" in English) is a traditional Okinawan martial arts weapon consisting of two sticks connected at one end by a short chain or rope.

# About

This project is an attempt to recreate the semi-popular templating framework, [Nunjucks](https://mozilla.github.io/nunjucks), in nim. The entire thing has been written completely from scratch, but also has been largely based off of the [original javascript implementation](https://github.com/mozilla/nunjucks).