# Package

version       = "0.1.0"
author        = "Chris Watson"
description   = "Nunjucks inspired templating for nim"
license       = "MIT"

# Dependencies

requires "nim >= 0.17.0"

