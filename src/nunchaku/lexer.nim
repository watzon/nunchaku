import parseutils, sequtils, strutils, tables, strtabs, re

type
    Tokenizer = ref object of RootObj
        src* : string
        str* : string
        index* : int
        len* : int
        lineno* : int
        colno* : int
        in_code* : bool

        trimBlocks* : bool
        lStripBlocks* : bool
        whitespace* : bool

    NToken* = enum
        T_STRING,
        T_WHITESPACE,
        T_DATA,
        T_BLOCK_START,
        T_BLOCK_END,
        T_VARIABLE_START,
        T_VARIABLE_END,
        T_COMMENT,
        T_LEFT_PAREN,
        T_RIGHT_PAREN,
        T_LEFT_BRACKET,
        T_RIGHT_BRACKET,
        T_LEFT_CURLY,
        T_RIGHT_CURLY,
        T_OPERATOR,
        T_COMMA,
        T_COLON,
        T_TILDE,
        T_PIPE,
        T_INT,
        T_FLOAT,
        T_BOOLEAN,
        T_NONE,
        T_SYMBOL,
        T_SPECIAL,
        T_REGEX

    Token* = object of RootObj
        kind* : NToken
        value* : string
        lineno* : int
        colno* : int

    Block* = tuple[name: string, startPos: int, endPos: int]

const
    WHITESPACE_CHARS* = "\n\t\r\u00A0\u0020\u200B"
    DELIM_CHARS* = "()[]{}%*-+~/#,:|.<>=!"
    INT_CHARS* = "1234567890"

    BLOCK_START* = "{%"
    BLOCK_END* = "%}"
    VARIABLE_START* = "{{"
    VARIABLE_END* = "}}"
    COMMENT_START* = "{#"
    COMMENT_END* = "#}"

proc newTokenizer*(src: string, trimBlocks: bool = false, lStripBlocks: bool = false, whitespace: bool = true): Tokenizer =
    result = Tokenizer()
    result.src = src
    result.str = strip(src)
    result.len = result.str.len
    result.index = 0
    result.lineno = 1
    result.colno = 1
    result.in_code = false

    result.trimBlocks = trimBlocks
    result.lStripBlocks = lStripBlocks
    result.whitespace = whitespace

proc newToken*(kind: NToken, lineno: int, colno: int): Token =
    result = Token()
    result.kind = kind
    result.lineno = lineno
    result.colno = colno

proc newToken*(kind: NToken, value: string, lineno: int, colno: int): Token =
    result = Token()
    result.kind = kind
    result.value = value
    result.lineno = lineno
    result.colno = colno

proc isNewline(c: char): bool =
    return NewLines.contains(c)

proc len*(t: Tokenizer): int =
    return t.str.len

proc isFinished*(t: Tokenizer): bool =
    return t.index >= t.len

proc current*(t: Tokenizer): char =
    if not t.isFinished:
        return t.str[t.index]
    return

proc currentStr*(t: Tokenizer): string =
    if not t.isFinished:
        return t.str.substr(t.index)
    return ""

proc previous*(t: Tokenizer): char =
    return t.str[t.index - 1]

proc forward*(t: var Tokenizer) =
    inc(t.index)

    if isNewline(t.previous):
        inc(t.lineno)
        t.colno = 0
    else:
        inc(t.colno)

proc forward*(t: var Tokenizer, n: int) =
    var i = 0
    while i < n:
        t.forward
        inc(i)

proc back*(t: var Tokenizer) =
    dec(t.index)

    if isNewline(t.current):
        dec(t.lineno)
        var idx = t.src.find("\n", t.index - 1)
        if idx == -1:
            t.colno = t.index
        else:
            t.colno = t.index - idx
    else:
        dec(t.colno)

proc back*(t: var Tokenizer, n: int) =
    var i = 0
    while i < n:
        t.back
        inc(i)

proc parseString*(t: var Tokenizer, delimiter: char): string =
    t.forward
    result = ""

    while not t.isFinished and t.current != delimiter:
        var cur = t.current
        
        if cur == '\\':
            t.forward
            case t.current:
            of 'n':
                result = result & "\n"
                break
            of 't':
                result = result & "\t"
                break
            of 'r':
                result = result & "\r"
            else:
                result = result & t.current
            t.forward
        else:
            result = result & cur
            t.forward

    t.forward

proc matches*(t: Tokenizer, str: string): bool =
    if t.index + str.len > t.len:
        return false
    
    var m = t.str[t.index .. t.index + str.len - 1]
    result = m == str

proc extractString*(t: var Tokenizer, str: string): string =
    if t.matches(str):
        inc(t.index, str.len)
        inc(t.colno, str.len)
        return str
    return ""

proc extractMatching*(t: var Tokenizer, breakOnMatch: bool, charString: string): string =
    if t.isFinished:
        return ""

    var first = charString.find(t.current)
    if (breakOnMatch and first == -1) or (not breakOnMatch and first != -1):
        var c: string = $t.current
        t.forward

        var idx = charString.find(t.current)

        while ((breakOnMatch and idx == -1) or (not breakOnMatch and idx != -1)) and (not t.isFinished):
            c = c & t.current
            t.forward

            idx = charString.find(t.current)
        
        return c
    return ""

proc extractUntil*(t: var Tokenizer, charString: string): string =
    return t.extractMatching(true, charString)

proc extract*(t: var Tokenizer, charString: string): string =
    return t.extractMatching(false, charString)

proc extractRegex*(t: var Tokenizer, regex: Regex): seq[string] =
    var matches = t.currentStr.findAll(regex)
    if (matches.len < 1):
        return nil
    
    t.forward(matches.len)
    return matches

iterator nextToken*(t: var Tokenizer): Token =
    var
        tok: string
    
    while t.index < t.len:
        if t.in_code:
            var cur = t.current

            if cur == '"' or cur == '\'':
                yield newToken(NToken.T_STRING, t.parseString(cur), t.lineno, t.colno)
            elif (tok = t.extract(WHITESPACE_CHARS); tok.len > 0):
                if (t.whitespace): # TODO: Figure out how to make this work
                    yield newToken(NToken.T_WHITESPACE, tok, t.lineno, t.colno)
            elif (tok = t.extractString(BLOCK_END); tok.len > 0) or (tok = t.extractString("-" & BLOCK_END); tok.len > 0):
                # Special check for the block end tag
                #
                # It is a requirement that start and end tags are composed of
                # delimiter characters (%{}[] etc), and our code always
                # breaks on delimiters so we can assume the token parsing
                # doesn't consume these elsewhere
                t.in_code = false
                if t.trimBlocks:
                    cur = t.current
                    if isNewline(cur):
                        t.forward
                yield newToken(NToken.T_BLOCK_END, tok, t.lineno, t.colno)
            elif (tok = t.extractString(VARIABLE_END); tok.len > 0) or (tok = t.extractString("-" & VARIABLE_END); tok.len > 0):
                # Special check for variable end tag (see above)
                t.in_code = false
                yield newToken(NToken.T_VARIABLE_END, tok, t.lineno, t.colno)
            elif (t.str[t.index .. t.index + 2] == "re\""):
                # Check for regular expressions. ({% set regExp = re"\\b(foo|bar)\\b" %})
                # Skip over "r/"
                t.forward(3)

                var regexBody = ""
                while not t.isFinished:
                    if t.current == '"' and t.previous != '\\':
                        t.forward
                        break
                    else:
                        regexBody &= t.current
                        t.forward
                
                yield newToken(NToken.T_REGEX, regexBody, t.lineno, t.colno)
            elif DELIM_CHARS.find(cur) != -1:
                # We've hit a delimiter (a special char like a bracket)
                t.forward
                var complexOps = @["==", "===", "!=", "!==", "<=", ">=", "//", "**"]
                var curComplex = cur & t.current
                var tokenType: NToken
                var value: string = $cur

                if complexOps.find(curComplex) != -1:
                    t.forward
                    value = curComplex

                    if complexOps.find(curComplex & t.current) != -1:
                        value = curComplex & t.current
                        t.forward

                case value:
                of "(": tokenType = NToken.T_LEFT_PAREN
                of ")": tokenType = NToken.T_RIGHT_PAREN
                of "[": tokenType = NToken.T_LEFT_BRACKET
                of "]": tokenType = NToken.T_RIGHT_BRACKET
                of "{": tokenType = NToken.T_LEFT_CURLY
                of "}": tokenType = NToken.T_RIGHT_CURLY
                else: tokenType = NToken.T_OPERATOR
                
                yield newToken(tokenType, value, t.lineno, t.colno)
            else:
                # We are not at whitespace or a delimiter, so extract
                # the text and parse it
                tok = t.extractUntil(WHITESPACE_CHARS & DELIM_CHARS)

                if tok.match(re"^[-+]?[0-9]+$"):
                    # Seems as though we've come upon a number
                    if t.current == '.':
                        # Float value
                        t.forward
                        var dec = t.extract(INT_CHARS)
                        yield newToken(NToken.T_FLOAT, tok & '.' & dec, t.lineno, t.colno)
                    else:
                        # Regular int
                        yield newToken(NToken.T_INT, tok, t.lineno, t.colno)
                elif tok.match(re"^(true|false)$"):
                    # Boolean value
                    yield newToken(NToken.T_BOOLEAN, tok, t.lineno, t.colno)
                elif tok == "none":
                    # None
                    yield newToken(NToken.T_NONE, tok, t.lineno, t.colno)
                else:
                    yield newToken(NToken.T_SYMBOL, tok, t.lineno, t.colno)
        else:
            # Parse out the template text, breaking on tag
            # delimiters because we need to look for block/variable start
            # tags (don't use the full delimChars for optimization)
            var beginChars = BLOCK_START[0] & VARIABLE_START[0] & COMMENT_START[0] & COMMENT_END[0]

            if (tok = t.extractString(BLOCK_START & '-'); tok.len > 1) or (tok = t.extractString(BLOCK_START); tok.len > 0):
                t.in_code = true
                yield newToken(NToken.T_BLOCK_START, tok, t.lineno, t.colno)
            elif (tok = t.extractString(VARIABLE_START & '-'); tok.len > 1) or (tok = t.extractString(VARIABLE_START); tok.len > 0):
                t.in_code = true
                yield newToken(NToken.T_VARIABLE_START, tok, t.lineno, t.colno)
            else:
                tok = ""
                var 
                    data: string
                    in_comment = false

                if t.matches(COMMENT_START):
                    in_comment = true
                    tok = t.extractString(COMMENT_START)
                
                # Continually consume text, breaking on the tag delimiter
                # characters and checking to see if it's a start tag.
                #
                # We could hit the end of the template in the middle of
                # our looping, so check for the null return value from
                # extractUntil
                while(data = t.extractUntil(beginChars); data != nil):
                    tok &= data

                    if (t.matches(BLOCK_START) or t.matches(VARIABLE_START) or t.matches(COMMENT_START) and not in_comment):
                        if (t.lStripBlocks and t.matches(BLOCK_START) and t.colno > 0 and t.colno <= tok.len):
                            var lastLine = tok[tok.len - t.colno .. ^1]
                            if lastLine.contains(re"^\s+$"):
                                # Remove block leading whitespace from beginning of the string
                                tok = tok[0 .. -t.colno]
                        break
                    elif t.matches(COMMENT_END):
                        if (in_comment):
                            raise newException(ValueError, "Unexpected end of comment")
                        tok &= t.extractString(COMMENT_END)
                        break
                    else:
                        # It does not match any tag, so add the character and
                        # carry on
                        tok &= t.current
                        t.forward
                
                if (data == nil and in_comment):
                    raise newException(ValueError, "Expected end of comment, got end of file")

                yield newToken(if in_comment: NToken.T_COMMENT else: NToken.T_DATA, tok, t.lineno, t.colno)

proc `$`*(t: Token): string =
    var value = if isNil(t.value): "nil" else: $t.value
    var line = t.lineno
    var col = t.colno
    return $t.kind & "#{ value: '" & value & "', line: " & $line & ", column: " & $col & " }"

if isMainModule:
    var src = """
{% for i in [1,2,3,4,5] -%}
    {{ i }}
{%- endfor %}"""

    var tokenizer = newTokenizer(src)

    for token in tokenizer.nextToken:
        echo(token)
