import json, typetraits, typeinfo

type
    Node* = ref object of RootObj
        lineno* : int
        colno* : int
    
    Value* = ref object of Node
        value* : string

    NodeList* = ref object of Node
        children* : seq[Node]
    
    Root* = ref object of NodeList
    Literal* = ref object of Value
    Symbol* = ref object of Value
    Group* = ref object of NodeList
    Array* = ref object of NodeList
    Dict* = ref object of NodeList
    Pair* = ref object of Node
        key* : string
        value* : string

    LookupVal* = ref object of Node
        target* : string
        val* : string

    If* = ref object of Node
        cond* : string
        body* : string
        elseCond* : string
    
    IfAsync* = ref object of If
    InlineIf* = ref object of Node
        cond* : string
        body* : string
        elseCond* : string

    For* = ref object of Node
        arr* : seq[string]
        name* : string
        body* : string
        elseCond* : string

    AsyncEach* = ref object of For
    AsyncAll* = ref object of For
    Macro* = ref object of Node
        name* : string
        args* : seq[string]
        body* : string

    Caller* = ref object of Macro
    Import* = ref object of Node
        templateName* : string
        target* : string
        withContext* : bool

    FromImport* = ref object of Node
        templateName* : string
        names* : seq[string]
        withContext* : bool

    FunCall* = ref object of Node
        name* : string
        args* : seq[string]

    Filter* = ref object of FunCall
    FilterAsync* = ref object of Filter
        symbol* : string

    KeywordArgs* = ref object of Dict
    Block* = ref object of Node
        name* : string
        body* : string

    Super* = ref object of Node
        blockName* : string
        symbol* : string

    TemplateRef* = ref object of Node
        templateName* : string

    Extends* = ref object of TemplateRef
    Include* = ref object of Node
        templateName* : string
        ignoreMissing* : bool

    Set* = ref object of Node
        targets* : seq[string]
        value* : string

    Output* = ref object of NodeList
    Capture* = ref object of Node
        body* : string

    TemplateData* = ref object of Literal
    UnaryOp* = ref object of Node
        target* : string

    BinOp* = ref object of Node
        left* : string
        right* : string

    In* = ref object of BinOp
    Or* = ref object of BinOp
    And* = ref object of BinOp
    Not* = ref object of BinOp
    Add* = ref object of BinOp
    Concat* = ref object of BinOp
    Sub* = ref object of BinOp
    Mul* = ref object of BinOp
    Div* = ref object of BinOp
    FloorDiv* = ref object of BinOp
    Mod* = ref object of BinOp
    Pow* = ref object of BinOp
    Neg* = ref object of UnaryOp
    Pos* = ref object of UnaryOp
    Compare* = ref object of Node
        expression* : string
        ops* : JsonNode

    CompareOperand* = ref object of Node
        expression* : string
        typeName* : string

    CallExtension* = ref object of Node
        extName* : string
        prop* : string
        args* : seq[string]
        contentArgs* : seq[string]

    CallExtensionAsync* = ref object of CallExtension

proc traverseAndCheck*(obj: Node, searchType: typedesc[Node], results: var seq[Node]) =
    if (obj is searchType):
        results.add(obj)
    if (obj is Node):
        obj.findAll(searchType, results)

proc findAll(n: Node, searchType: typedesc[Node], results: var seq[Node]) =
    for name, value in fieldPairs(n):
        traverseAndCheck(name, searchType, results)

proc newNode*(kind: Node, lineno: int, colno: int): Node =
    result = kind
    result.lineno = lineno
    result.colno = colno

proc newNode*(kind: Value, lineno: int, colno: int, value: string): Value =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.value = value

proc newNode*(kind: NodeList, lineno: int, colno: int, children: seq[Node]): NodeList =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.children = children

proc newNode*(kind: Pair, lineno: int, colno: int, key: string, value: string): Pair =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.key = key
    result.value = value

proc newNode*(kind: LookupVal, lineno: int, colno: int, target: string, val: string): LookupVal =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.target = target
    result.val = val

proc newNode*(kind: If, lineno: int, colno: int, cond: string, body: string, elseCond: string): If =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.cond = cond
    result.body = body
    result.elseCond = elseCond

proc newNode*(kind: InlineIf, lineno: int, colno: int, cond: string, body: string, elseCond: string): InlineIf =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.cond = cond
    result.body = body
    result.elseCond = elseCond

proc newNode*(kind: For, lineno: int, colno: int, arr: seq[string], name: string, body: string, elseCond: string): For =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.arr = arr
    result.name = name
    result.body = body
    result.elseCond = elseCond

proc newNode*(kind: Macro, lineno: int, colno: int, name: string, args: seq[string], body: string): Macro =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.name = name
    result.args = args
    result.body = body

proc newNode*(kind: Import, lineno: int, colno: int, templateName: string, target: string, withContext: bool): Import =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.templateName = templateName
    result.target = target
    result.withContext = withContext

proc newNode*(kind: FromImport, lineno: int, colno: int, templateName: string, names: seq[string], withContext: bool): FromImport =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.templateName = templateName
    result.names = names
    result.withContext = withContext

proc newNode*(kind: FunCall, lineno: int, colno: int, name: string, args: seq[string]): FunCall =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.name = name
    result.args = args

proc newNode*(kind: FilterAsync, lineno: int, colno: int, symbol: string): FilterAsync =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.symbol = symbol

proc newNode*(kind: Block, lineno: int, colno: int, name: string, body: string): Block =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.name = name
    result.body = body

proc newNode*(kind: Super, lineno: int, colno: int, blockName: string, symbol: string): Super =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.blockName = blockName
    result.symbol = symbol

proc newNode*(kind: TemplateRef, lineno: int, colno: int, templateName: string): TemplateRef =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.templateName = templateName

proc newNode*(kind: Include, lineno: int, colno: int, templateName: string, ignoreMissing: bool): Include =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.templateName = templateName
    result.ignoreMissing = ignoreMissing

proc newNode*(kind: Set, lineno: int, colno: int, targets: seq[string], value: string): Set =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.targets = targets
    result.value = value

proc newNode*(kind: Capture, lineno: int, colno: int, body: string): Capture =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.body = body

proc newNode*(kind: UnaryOp, lineno: int, colno: int, target: string): UnaryOp =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.target = target

proc newNode*(kind: BinOp, lineno: int, colno: int, left: string, right: string): BinOp =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.left = left
    result.right = right

proc newNode*(kind: Compare, lineno: int, colno: int, expression: string, ops: JsonNode): Compare =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.expression = expression
    result.ops = ops

proc newNode*(kind: CompareOperand, lineno: int, colno: int, expression: string, typeName: string): CompareOperand =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.expression = expression
    result.typeName = typeName

proc newNode*(kind: CallExtension, lineno: int, colno: int, extName: string, prop: string, args: seq[string] = @[], contentArgs: seq[string] = @[]): CallExtension =
    result = kind
    result.lineno = lineno
    result.colno = colno
    result.extName = extName
    result.prop = prop
    result.args = args
    result.contentArgs = contentArgs

if isMainModule:
    var n = newNode(Value(), 0, 0, "Hello")
    var results: seq[Node] = @[]

    traverseAndCheck(n, Value, results)

    echo(repr(results))